import get from 'lodash/get'
import isEmpty from 'lodash/isEmpty'
import { applySnapshot, types } from 'mobx-state-tree'
import userStore from './user_store'
import { isServer } from '../lib_shared/constants'

let store = null

const RootStore = types.model({
  userStore: types.optional(userStore, {})
})
  .views(self => ({}))
  .actions(self => ({}))

export const initStore = (snapshot = null) => {
  if (isServer) {
    store = RootStore.create()
  }

  if (isEmpty(store)) {
    const cache = (!isServer && get(window, '__NEXT_DATA__.props.pageProps.initialState')) || {}
    store = RootStore.create(cache)
  }

  if (snapshot) applySnapshot(store, snapshot)

  return store
}
