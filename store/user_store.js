import isEmpty from 'lodash/isEmpty'
import { types, flow } from 'mobx-state-tree'
import { baseUrl, isServer } from '../lib_shared/constants'
import fetch from '../lib_shared/fetch'

export default types
  .model({
    createDate: types.optional(types.string, ''),
    description: types.maybeNull(types.string, ''),
    email: types.optional(types.string, ''),
    firstName: types.maybeNull(types.string, ''),
    id: types.optional(types.number, 0),
    lastName: types.maybeNull(types.string, '')
  })
  .views(self => ({
    get isSignedIn () { return !isEmpty(self.email) }
  }))
  .actions(self => {
    const getUser = flow(function * () {
      const url = isServer ? `${baseUrl}/api/users/me` : '/api/users/me'
      const userInfoResp = yield fetch(url)
      if (userInfoResp.ok) {
        const { user } = yield userInfoResp.json()
        return user
      }
    })

    const setUser = user => {
      self.createDate = user.createDate
      self.description = user.description
      self.email = user.email
      self.firstName = user.firstName
      self.id = user.id
      self.lastName = user.lastName
    }

    const updateUser = flow(function * (user) {
      const url = isServer ? `${baseUrl}/api/users/me` : '/api/users/me'
      const body = JSON.stringify(user)
      const headers = { 'Content-Type': 'application/json' }
      const userInfoResp = yield fetch(url, { body, headers, method: 'PUT' })
      if (userInfoResp.ok) {
        const { user } = yield userInfoResp.json()
        return user
      }
    })

    return {
      getUser,
      setUser,
      updateUser
    }
  })
