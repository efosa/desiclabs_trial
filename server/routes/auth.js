const express = require('express')
const fetch = require('fetch-retry')
const _ = require('lodash')
const ms = require('ms')
const { getUserByEmail, upsertUser } = require('../db/users')
const authUtils = require('../../lib_shared/auth')
const { baseUrl, githubClientId, githubClientSecret } = require('../../lib_shared/constants')

const router = express.Router()
const AUTH_COOKIE_NAME = 'access_token'

const formatUrl = url => url
  .replace(/(\r\n|\n|\r)/gm, '')
  .replace(/\s/g, '')

router.get('/auth/github', (req, res) => {
  const url = formatUrl(`https://github.com/login/oauth/authorize?
    &access_type=offline
    &redirect_uri=${baseUrl}/auth/github/callback
    &allow_signup=true
    &client_id=${githubClientId}`)
  res.redirect(url)
})

router.get('/auth/github/callback', async (req, res) => {
  const code = _.get(req, 'query.code')
  const url = formatUrl(`
    https://github.com/login/oauth/access_token?
    client_id=${githubClientId}
    &client_secret=${githubClientSecret}
    &code=${code}
    &redirect_uri=${baseUrl}/auth/github/callback
  `)

  const tokenResp = await fetch(url, { method: 'POST' })

  let token = null
  let error = null

  if (tokenResp.ok) {
    const tokenRespData = await tokenResp.text()
    const isValidToken = _.includes(tokenRespData, 'access_token=')
    if (isValidToken) token = tokenRespData
    else error = tokenRespData
  } else {
    error = tokenResp.statusText
  }

  if (error) return res.redirect('/login?error=github_auth_error')

  const userResp = await fetch(`https://api.github.com/user?${token}`)

  if (!userResp.ok) res.redirect(`/login?error=${userResp.statusText}`)

  // fetch github user info
  const user = await userResp.json()

  loginGithubUser(user, res)
})

const loginGithubUser = async (user, res) => {
  const email = user.email || `${user.login}@github-user.com`
  let userRecord = await getUserByEmail(email)
  const foundUserInDb = !_.isEmpty(userRecord.user)

  // register user if record is not found
  const modifiedGithubUserRec = { ...user }
  modifiedGithubUserRec['email'] = email
  console.log('modifiedGithubUserRec DEBUGGING', modifiedGithubUserRec)
  if (!foundUserInDb) userRecord = await registerNewGithubUser(modifiedGithubUserRec)

  createAuthCookieAndRedirect(userRecord, res)
}

const registerNewGithubUser = async userInfo => {
  const { email, name } = userInfo
  const [ firstName = '', lastName = '' ] = name.split(' ')
  return upsertUser(email, firstName, lastName)
}

const createAuthCookieAndRedirect = (userRecord, res) => {
  const encodedToken = authUtils.encodeJwtToken(userRecord)
  res.cookie(AUTH_COOKIE_NAME, encodedToken, { httpOnly: true, maxAge: ms('1yr') })
  return res.redirect(baseUrl)
}

router.get('/signout', (req, res) => {
  res.clearCookie(AUTH_COOKIE_NAME)
  res.redirect(baseUrl)
})

module.exports = router
