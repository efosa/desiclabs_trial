const express = require('express')
const _ = require('lodash')
const { getUserByEmail, upsertUser } = require('../db/users')

const router = express.Router()
const UNAUTH_JSON_RESP = { error: 'Unauthorized', httpStatusCode: 401 }

router.get('/users/me', async (req, res) => {
  const secureAuthEmail = _.get(res, 'locals.auth_email')
  if (!secureAuthEmail) return res.status(401).json(UNAUTH_JSON_RESP)

  const resp = await getUserByEmail(secureAuthEmail)
  const userNotFound = _.isEmpty(resp.user)

  if (userNotFound) {
    return res
      .status(401)
      .json({ error: 'Not Found', httpStatusCode: 404 })
  }

  return res.json(resp)
})

router.put('/users/me', async (req, res) => {
  const secureAuthEmail = _.get(res, 'locals.auth_email')
  const { body = {} } = req
  const { description, email, firstName, lastName } = body

  const isValidRequest = secureAuthEmail && (email === secureAuthEmail)
  if (!isValidRequest) return res.status(401).json(UNAUTH_JSON_RESP)

  const resp = await upsertUser(email, firstName, lastName, description)
  const userNotFound = _.isEmpty(resp.user)

  if (userNotFound) {
    return res
      .status(401)
      .json({ error: 'Not Found', httpStatusCode: 404 })
  }

  return res.json(resp)
})

module.exports = router
