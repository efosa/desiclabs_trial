const { Pool } = require('pg')

const {
  PG_DB: database,
  PG_HOST: host,
  PG_PORT: port,
  PG_USER: user,
  PG_PASSWORD: password
} = process.env

const pool = new Pool({
  database,
  host,
  password,
  port: port || 5432,
  user
})

module.exports = pool
