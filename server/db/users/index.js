const { camelizeKeys } = require('humps')
const db = require('../client')

const getAll = async () => {
  try {
    const text = `
      SELECT * from users
    `
    const { rows } = await db.query(text)
    const users = camelizeKeys(rows)
    return { users }
  } catch (err) {
    return { error: err.message }
  }
}

const getUserByEmail = async (email) => {
  try {
    const text = `
      SELECT * from users
      WHERE email = $1
    `
    const values = [email]
    const { rows } = await db.query(text, values)
    const user = camelizeKeys(rows)[0]
    return { user }
  } catch (err) {
    return { error: err.message }
  }
}

const upsertUser = async (email, firstName = '', lastName = '', description = '') => {
  try {
    const values = [email, firstName, lastName, description]
    const text = `
      INSERT INTO users (email, first_name, last_name, description)
      VALUES ($1, $2, $3, $4)
      ON CONFLICT (email)
      DO
      UPDATE
        SET
          first_name = EXCLUDED.first_name,
          last_name = EXCLUDED.last_name,
          description = EXCLUDED.description
      RETURNING *
      ;
      `
    const { rows } = await db.query(text, values)
    const user = camelizeKeys(rows)[0]
    return { user }
  } catch (error) {
    return { error, type: 'brand insert' }
  }
}

module.exports = {
  getAll,
  getUserByEmail,
  upsertUser
}
