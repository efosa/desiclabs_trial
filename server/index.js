require('dotenv').config()
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const express = require('express')
const mobxReact = require('mobx-react')
const next = require('next')
const setAuthEmail = require('./middleware/setAuthEmail')
const apiRoutes = require('./routes/api')
const authRoutes = require('./routes/auth')
const { isDev } = require('../lib_shared/constants')

mobxReact.useStaticRendering(true)

const port = parseInt(process.env.PORT, 10) || 3000
const server = next({ dev: isDev })

server.prepare()
  .then(() => {
    const handle = server.getRequestHandler()
    const app = express()

    app.use(bodyParser.json())
    app.use(cookieParser())
    app.use(setAuthEmail)
    app.use('/api', apiRoutes)
    app.use(authRoutes)
    app.get('*', (req, res) => handle(req, res))

    app.listen(port, (err) => {
      if (err) throw err
      const mode = isDev ? 'DEVELOPMENT' : 'PRODUCTION'
      console.log(`> Server is ready on http://localhost:${port} - [${mode}]`)
    })
  })
