const _ = require('lodash')
const { decodeJwtToken } = require('../../lib_shared/auth')

const setAuthEmail = (req, res, next) => {
  const { cookies } = req
  const accessToken = _.get(cookies, 'access_token')
  if (accessToken) {
    const { user = {} } = decodeJwtToken(accessToken)
    const { email } = user
    if (email) res.locals.auth_email = email
  }
  next()
}

module.exports = setAuthEmail
