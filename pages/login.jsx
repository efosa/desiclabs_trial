import injectSheet from 'react-jss'
import { GithubLoginButton } from 'react-social-login-buttons'
import Layout from '../components/layout'

const styles = {
  root: {
    width: '100%'
  },
  controls: {
    display: 'flex',
    justifyContent: 'center'
  }
}

const Page = ({ classes }) => {
  return (
    <Layout>
      <div className={classes.root}>
        <h1>Login</h1>
        <section className={classes.controls}>
          <a href='/auth/github'>
            <GithubLoginButton />
          </a>
        </section>
      </div>
    </Layout>
  )
}

export default injectSheet(styles)(Page)
