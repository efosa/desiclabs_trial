import Layout from '../components/layout'
import injectSheet from 'react-jss'
import { baseUrl } from '../lib_shared/constants'
import Router from 'next/router'

const styles = {
  root: {
    '& img': {
      maxWidth: '100%'
    }
  }
}

const Page = ({ classes }) => {
  return (
    <Layout>
      <div className={classes.root}>
        <h1>Secret Area</h1>
        <img
          alt='Private Image'
          src='/static/images/adv-of-business-cat.png'
        />
      </div>
    </Layout>
  )
}

Page.getInitialProps = ({ res, store }) => {
  const { userStore } = store
  const { isSignedIn } = userStore
  if (!isSignedIn) {
    if (res) return res.redirect(`${baseUrl}/login`)
    Router.push('/login')
  }
}

export default injectSheet(styles)(Page)
