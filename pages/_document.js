import Document, { Head, Main, NextScript } from 'next/document'
import React from 'react'
import { JssProvider, SheetsRegistry } from 'react-jss'

class AppDocument extends Document {
  static async getInitialProps (ctx) {
    const { renderPage } = ctx
    const initialProps = await Document.getInitialProps(ctx)
    const sheets = new SheetsRegistry()

    const decoratePage = Page => props =>
      <JssProvider registry={sheets}>
        <Page {...props} />
      </JssProvider>

    const renderedPage = renderPage(decoratePage)

    const styles = (
      <style
        dangerouslySetInnerHTML={{ __html: sheets.toString() }}
        id='server-side-styles'
        type='text/css'
      />
    )

    return { ...initialProps, ...renderedPage, styles }
  }

  render () {
    return (
      <html>
        <Head>
          <style dangerouslySetInnerHTML={{
            __html: `
              html, body {
                font-family: 'Raleway', arial, helvetica;
                min-width: 350px;
              }

              * {
                  box-sizing: border-box;
                  margin: 0;
                  padding: 0;
              }

              a {
                color: #666;
                text-decoration: none;
              }

              h2: {
                margin: 10px 0
              }

              .container {
                margin: 0 auto;
                max-width: 1180px;
                width: 100%;
              }
          ` }}
          />
          <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}

export default AppDocument
