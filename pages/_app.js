import get from 'lodash/get'
import { Provider } from 'mobx-react'
import { getSnapshot } from 'mobx-state-tree'
import Router from 'next/router'
import { Component } from 'react'
import RouterProvider from '../components/router_provider'
import { initStore } from '../store'
import { decodeJwtToken } from '../lib_shared/auth'

const Container = props => props.children

class WithMobx extends Component {
  static async getInitialProps ({ Component, ctx }) {
    const { req } = ctx
    const store = initStore()
    await WithMobx.loadUserFromToken(store, req)
    let pageProps = {}
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps({ ...ctx, store })
    }

    const initialState = { ...getSnapshot(store) }
    return { initialState, pageProps }
  }

  constructor (props) {
    super(props)
    this.store = this.getStore()
  }

  static async loadUserFromToken (store, req) {
    const { userStore } = store
    if (req) {
      const accessToken = get(req, 'cookies.access_token')
      if (accessToken) {
        const { user = {} } = decodeJwtToken(accessToken)
        const { email } = user
        if (email) await userStore.setUser(user)
      }
    }
  }

  getStore = () => {
    const { initialState } = this.props
    const store = initStore(initialState)
    return store
  }

  render () {
    const { Component, pageProps } = this.props

    return (
      <RouterProvider router={Router}>
        <Container>
          <Provider store={this.store}>
            <Component {...pageProps} />
          </Provider>
        </Container>
      </RouterProvider>
    )
  }
}

export default WithMobx
