import Router from 'next/router'
import injectSheet from 'react-jss'
import Layout from '../components/layout'
import Profile from '../components/profile'
import { baseUrl } from '../lib_shared/constants'

const styles = {
  root: {
    '& img': {
      maxWidth: '100%'
    }
  }
}

const Page = ({ classes }) => {
  return (
    <Layout>
      <div className={classes.root}>
        <h1>My Profile</h1>
        <Profile />
      </div>
    </Layout>
  )
}

Page.getInitialProps = ({ res, store }) => {
  const { userStore } = store
  const { isSignedIn } = userStore
  if (!isSignedIn) {
    if (res) return res.redirect(`${baseUrl}/login`)
    Router.push('/login')
  }
}

export default injectSheet(styles)(Page)
