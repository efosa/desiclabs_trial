const path = require('path')
const webpack = require('webpack')
const withCSS = require('@zeit/next-css')
const { isDev } = require('./lib_shared/constants')

if (isDev) require('dotenv').config()

module.exports = withCSS({
  distDir: '_build',
  // target: 'serverless',
  // Will be available on both server and client
  publicRuntimeConfig: { staticFolder: '/static' },
  webpack: (config, { buildId, dev }) => {
    const emptyShim = path.resolve(__dirname, 'lib_shared/emptyShim.js')
    const alias = config.resolve.alias || {}

    // build aliases
    alias['child_process'] = emptyShim
    alias['dns'] = emptyShim
    alias['dtrace-provider'] = emptyShim
    alias['fs'] = emptyShim
    alias['mv'] = emptyShim
    alias['net'] = emptyShim
    alias['pg-native'] = emptyShim
    alias['tls'] = emptyShim
    alias['safe-json-stringify'] = emptyShim
    alias['source-map-support'] = emptyShim

    config.resolve.alias = alias

    const frontendConstants = new webpack.DefinePlugin({
      'process.env.BUILD_ID': JSON.stringify(buildId),
      'process.env.LOG_LEVEL': JSON.stringify(process.env.LOG_LEVEL),
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    })

    config.plugins.push(frontendConstants)

    return config
  }
})
