# Features

* Universal react app
* Centralized & typed store (mobx state tree)
* CSS modules (react-jss)
* Postgres Database backend
* Internal secure auth via server-only JWT tokens
* _Custom_ Github OAUTH implementation

# TODO

1. ~~allow user to update their profile~~ - done
2. ~~add secondary OATH implementation~~ - Another time ... Zzz
3. ~~deploy site in secure test environment~~ - done

# How to run project

1. `npm install`.
2. add `.env` file to project dir.
3. start project with `npm run dev` command.

### Page Routes

- / (secured)
- /login
- /profile (secured)

### API routes

- /users/me (secured)
- /auth/github
- /auth/github/callback
- /signout (deletes JWT auth cookie)
