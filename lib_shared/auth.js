const jwt = require('jwt-simple')
const { jwtSecret } = require('../lib_shared/constants')

const decodeJwtToken = token => jwt.decode(token, jwtSecret)
const encodeJwtToken = token => jwt.encode(token, jwtSecret)

module.exports = {
  decodeJwtToken,
  encodeJwtToken
}
