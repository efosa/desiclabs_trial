const isServer = typeof window === 'undefined'
const isProd = process.env.NODE_ENV === 'production'

// server only secret
const jwtSecret = isServer ? Buffer.from(process.env.JWT_SECRET).toString('base64') : null

module.exports = {
  baseUrl: process.env.BASE_URL,
  githubClientId: process.env.GITHUB_CLIENT_ID,
  githubClientSecret: process.env.GITHUB_CLIENT_SECRET,
  isClient: !isServer,
  isDev: !isProd,
  isProd,
  isServer,
  jwtSecret,
  longName: 'Desic Labs Inc.',
  shortName: 'Desic Labs'
}
