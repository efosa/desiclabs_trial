import { Formik } from 'formik'
import { inject, observer } from 'mobx-react'
import { Component } from 'react'
import injectSheet from 'react-jss'

const styles = {
  root: {
    '& button': {
      border: 'none',
      borderRadius: '6px',
      cursor: 'pointer',
      fontSize: '18px',
      outline: 'none',
      padding: '10px 25px',
      '&:hover': {
        background: '#CCC'
      }
    },
    '& input, & textarea': {
      display: 'flex',
      fontSize: '18px',
      padding: '4px'
    },
    '& textarea': {
      maxWidth: 'calc(100vw - 20px)',
      width: '450px'
    },
    '& form label': {
      color: '#666',
      display: 'block',
      margin: '15px 0 5px'
    }
  },
  btnRow: {
    marginTop: '15px'
  },
  btnSubmit: {
    background: 'lightgreen',
    marginLeft: '10px',
    '&:hover': {
      background: '#4be44b!important'
    }
  }
}

class Profile extends Component {
  state = {
    user: undefined
  }

  async componentDidMount () {
    const { store } = this.props
    const { userStore } = store
    const { getUser } = userStore
    const user = await getUser()
    this.updateUserState(user)
  }

  updateUserState = user => {
    this.setState({ user })
  }

  render () {
    const {
      classes,
      store
    } = this.props

    const { user } = this.state
    if (!user) return <p>Loading ...</p>

    const { email, firstName, description, lastName } = user
    const { userStore } = store

    const handleOnSubmit = values => userStore.updateUser(values)

    return (
      <div className={classes.root}>
        <Formik
          initialValues={{ email, firstName, description, lastName }}
          onSubmit={async (values, actions) => {
            await handleOnSubmit(values)
            this.updateUserState(values)
            actions.setSubmitting(false)
          }}
          render={props => (
            <form onSubmit={props.handleSubmit}>
              <label>First Name</label>
              <input
                name='firstName'
                onBlur={props.handleBlur}
                onChange={props.handleChange}
                type='text'
                value={props.values.firstName}
              />
              <label>Last Name</label>
              <input
                name='lastName'
                onBlur={props.handleBlur}
                onChange={props.handleChange}
                type='text'
                value={props.values.lastName}
              />
              <label>Email</label>
              <input
                disabled
                name='email'
                type='text'
                value={props.values.email}
              />
              <label>Description</label>
              <textarea
                name='description'
                onBlur={props.handleBlur}
                onChange={props.handleChange}
                rows={5}
                value={props.values.description}
              />

              <section className={classes.btnRow}>
                <button onClick={props.handleReset} type='reset'>Reset</button>
                <button className={classes.btnSubmit} type='submit'>
                  {props.isSubmitting ? 'Saving ...' : 'Save Profile'}
                </button>
              </section>
            </form>
          )}
        />
      </div>
    )
  }
}

export default injectSheet(styles)(inject('store')(observer(Profile)))
