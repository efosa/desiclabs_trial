import Head from 'next/head'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import injectSheet from 'react-jss'
import Footer from './footer'
import Header from './header'

const styles = {
  root: {
    alignItems: 'flex-start',
    background: '#F5F5F5',
    display: 'flex',
    justifyContent: 'center',
    minHeight: 'calc(100vh - 125px)',
    '& h1': {
      fontWeight: 'normal',
      fontSize: '30px',
      lineHeight: '2em',
      textAlign: 'left',
      width: 'auto',
      '& a': {
        color: '#755'
      }
    }
  },
  wrapper: {
    display: 'flex',
    maxWidth: '1180px',
    padding: '0 10px',
    width: '100%'
  },
  '@media (max-height: 768px)': {
    minHeight: 0
  }
}

class Layout extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.array.isRequired,
      PropTypes.object.isRequired
    ]),
    classes: PropTypes.object.isRequired,
    showFooter: PropTypes.bool,
    title: PropTypes.string
  }

  static defaultProps = {
    showFooter: true
  }

  componentDidMount () {
    // remove SSR (server-side rendered) JSS styles when page mounts.
    // this avoids duplicate styles, since the browser will regenerate
    // the same styles on the client
    const ssrStyles = document.getElementById('server-side-styles')
    ssrStyles && ssrStyles.parentNode.removeChild(ssrStyles)
  }

  render () {
    const {
      children,
      classes,
      showFooter,
      title
    } = this.props

    const defaultTitle = 'Desic Labs'
    const fullTitle = title ? `${title} - ${defaultTitle}` : defaultTitle

    return (
      <div id='root'>
        <Head>
          <title>{fullTitle}</title>
          <meta charSet='utf-8' />
          <meta
            content='width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=yes'
            name='viewport'
          />
          <meta content='text/html; charset=UTF-8' httpEquiv='Content-Type' />
          <meta content='width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, target-densityDpi=device-dpi' name='viewport' />

          <link href='data:image/x-icon;base64,AAABAAEAEBAAAAAAAABoBQAAFgAAACgAAAAQAAAAIAAAAAEACAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAA/96zAPrWpwD/4boAramjAP/crQCWlZMA8MaQAPXNmgD60p0AjIqHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoECgMDAQIJCAgHCgAKAAAKAAoGBgYGBgYGBgoECgAACgQKCQgIBwcICAgKAAoAAAoACgkJCAgHBwgICgQKAAAKBAoBCQkICAcHCAoACgAACgAKAwEJCQgIBwcKBAoAAAoECgMDAQkJCAgHCgAKAAAKAAoGBgYGBgYGBgoECgAACgQKCQgIBwcICAgKAAoAAAoACgkJCAgHBwgICgQKAAAKBAoBCQkICAcHCAoACgAACgAKAwUJCQgIBwcKBAoAAAoECgMDAQIJCAgHCgAKAAAKAAoGBgYGBgYGBgoECgAACgQKCQgIBwcICAgKAAoAAAoACgkJCAgHBwgICgQKAIAFAACgAQAAgAUAAKABAACABQAAoAEAAIAFAACgAQAAgAUAAKABAACABQAAoAEAAIAFAACgAQAAgAUAAKABAAA=' rel='icon' type='image/x-icon' />
        </Head>
        <Header />
        <div className={classes.root}>
          <div className={classes.wrapper}>
            {children}
          </div>
        </div>
        {showFooter ? <Footer /> : null}
      </div>
    )
  }
}

export default injectSheet(styles)(Layout)
