import Link from 'next/link'
import PropTypes from 'prop-types'
import { Component } from 'react'
import injectSheet from 'react-jss'
import HeaderNav from '../header_nav'
import { shortName } from '../../lib_shared/constants'

const styles = {
  root: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    minHeight: '100px',
    padding: '5px 5px 5px 0',
    width: '100%',
    '& h1': {
      color: '#FAA',
      fontSize: '30px',
      margin: '0 10px 0 5px',

      '& span': {
        color: '#FFF'
      }
    }
  },
  logo: {
    fontWeight: 800,
    '& img': {
      display: 'inline-flex',
      maxHeight: '40px'
    },
    '& a': {
      color: 'orange',
      textDecoration: 'none'
    }
  },
  wrapper: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    maxWidth: '1180px',
    width: '100%'
  }
}

class Header extends Component {
  render () {
    const { classes } = this.props
    return (
      <div className={classes.root}>
        <div className={classes.wrapper}>
          <h1 className={classes.logo}>
            <Link href='/'>
              <a>
                <img
                  alt={shortName}
                  src='static/images/logo.png'
                />
              </a>
            </Link>
          </h1>
          <span style={{ flexGrow: 1 }} />
          <HeaderNav />
        </div>
      </div>

    )
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired
}

export default injectSheet(styles)(Header)
