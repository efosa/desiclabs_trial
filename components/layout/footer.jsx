import PropTypes from 'prop-types'
import injectSheet from 'react-jss'
import { longName } from '../../lib_shared/constants'

const styles = {
  root: {
    alignItems: 'center',
    background: '#f5f5f5',
    color: '#333',
    display: 'flex',
    flexDirection: 'column',
    minHeight: 25,
    justifyContent: 'center'
  }
}

const Footer = ({ classes }) => {
  const currentYear = new Date().getFullYear()

  return (
    <footer className={classes.root}>
      {longName} Copyright &copy; {currentYear}
    </footer>
  )
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired
}

export default injectSheet(styles)(Footer)
