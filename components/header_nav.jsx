import { inject, observer } from 'mobx-react'
import Link from 'next/link'
import {
  FaHome,
  FaSignInAlt,
  FaSignOutAlt,
  FaUser
} from 'react-icons/fa'
import injectSheet from 'react-jss'

const styles = {
  root: {
    color: 'red',
    display: 'flex',
    listStyleType: 'none',
    '& li a': {
      alignItems: 'center',
      borderLeft: '1px solid rgba(0, 0, 0, 0.1)',
      display: 'inline-flex',
      fontSize: 24,
      fontWeight: 700,
      height: '84px',
      padding: '0 25px',
      transition: 'all 0.5s',
      '&:hover': {
        color: '#00ACED'
      }
    },
    '& span.mobile': {
      display: 'none'
    }
  },
  '@media (max-width: 780px)': {
    root: {
      '& span.desktop': {
        display: 'none'
      },
      '& span.mobile': {
        display: 'block'
      }
    }
  }
}

const HeaderNav = ({ classes, store }) => {
  const { userStore } = store
  const { isSignedIn } = userStore
  const loginBtn = isSignedIn ? null : (
    <li>
      <Link href='/login'><a>
        <span className='desktop'>Login</span>
        <span className='mobile'><FaSignInAlt /></span>
      </a></Link></li>
  )
  const homeBtn = isSignedIn ? (
    <li><Link href='/'><a>
      <span className='desktop'>Home</span>
      <span className='mobile'><FaHome /></span>
    </a></Link></li>
  ) : null

  const profileBtn = isSignedIn ? (
    <li><Link href='/profile'><a>
      <span className='desktop'>My Profile</span>
      <span className='mobile'><FaUser /></span>
    </a></Link></li>
  ) : null
  const signOutBtn = isSignedIn ? (
    <li><Link href='/signout'><a>
      <span className='desktop'>Sign Out</span>
      <span className='mobile'><FaSignOutAlt /></span>
    </a></Link></li>
  ) : null

  return (
    <ul className={classes.root}>
      {homeBtn}
      {loginBtn}
      {profileBtn}
      {signOutBtn}
    </ul>
  )
}

export default injectSheet(styles)(inject('store')(observer(HeaderNav)))
